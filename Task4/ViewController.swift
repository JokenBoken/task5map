//
//  ViewController.swift
//  Task4
//
//  Created by Евгений Григоренко on 4.10.21.
//

import UIKit
import MapKit
import CoreLocation


class ViewController: UIViewController {
    
    let mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        return mapView
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(mapView)
        mapView.delegate = self
        let coordinateRegion = CLLocation(latitude: 52.425163, longitude: 31.015039)
        location(coordinateRegion)
        allBanksJSON(id: "filial_id",
                          url: "https://belarusbank.by/api/filials_info",
                          city: "name",
                          gps_x: "GPS_X",
                          gps_y: "GPS_Y",
                          address: "street")
        allBanksJSON(id: "id",
                          url: "https://belarusbank.by/api/atm",
                          city: "city",
                          gps_x: "gps_x",
                          gps_y: "gps_y",
                          address: "address")
        allBanksJSON(id: "info_id",
                            url: "https://belarusbank.by/api/infobox",
                          city: "city",
                          gps_x: "gps_x",
                          gps_y: "gps_y",
                          address: "address")

        self.constraint()
    }
    
    func location(_ location: CLLocation, locationRadius: CLLocationDistance = 1500){
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate,
                                                      latitudinalMeters: locationRadius,
                                                      longitudinalMeters: locationRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func constraint(){
        mapView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        mapView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        mapView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}

extension ViewController: MKMapViewDelegate{
    func allBanksJSON(id: String, url: String, city: String, gps_x: String, gps_y: String, address: String){
        guard let url = URL(string: url) else {return}
        let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataAnswer = data, error == nil else {return}
            do{
                let jsonAnswer = try JSONSerialization.jsonObject(with: dataAnswer, options: [])
                guard let jsonArray = jsonAnswer as? [[String: Any]] else {
                    return
                }
                var dictionaries: [[String : Any]] = [[:]]
                var distances: [(id: String, adress: String, x: Double, y: Double, dist: CLLocationDistance)] = []
                for dictionary in jsonArray{
                    dictionaries = [[:]]
                    var allLocationArray: [CLLocation] = []
                    if dictionary[city] as? String == "Гомель" {
                        dictionaries.append(dictionary)
                        guard let doubleX = Double(dictionary[gps_x] as? Substring ?? "") else { return }
                        guard let doubleY = Double(dictionary[gps_y] as? Substring ?? "") else { return }
                        var intId = String()
                        if dictionary[id] is Int{
                            intId = String(dictionary[id] as? Int ?? 0)
                        }
                        else{
                            intId = dictionary[id] as? String ?? ""
                        }
                        let location = CLLocation(latitude : doubleX, longitude: doubleY)
                        allLocationArray.append(location)
                        for locationBank in allLocationArray{
                            let staticLocation = CLLocation(latitude: 52.425163, longitude: 31.015039)
                            let distance = locationBank.distance(from: staticLocation)
                            distances.append((id: intId, adress: (dictionary[address] as! String), x: doubleX, y: doubleY, dist: distance))
                            print(distances)
                        }
                    }
                    
                }
                var annotations: [MKPointAnnotation] = []
                for (_, adress, x, y, dist) in distances {
                    if dist <= 1000 {
                        let annotation = MKPointAnnotation()
                        annotation.title = adress
                        annotation.coordinate = CLLocationCoordinate2D(latitude : x, longitude: y)
                        annotations.append(annotation)
                    }
                }
                DispatchQueue.main.sync { self.mapView.addAnnotations(annotations) }
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        dataTask.resume()
    }
}

